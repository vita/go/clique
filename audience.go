package clique

import (
	"time"

	"github.com/google/uuid"
)

type (
	// Audience represents a group of users who can view some shared content.
	Audience struct {
		ID          uuid.UUID `json:"id"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		Owner       *User     `json:"owner"`
		Members     []User    `json:"members"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	}
)
