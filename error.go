package clique

type (
	// Error is used to process errors returned by the Clique API.
	Error struct {
		Code        string `json:"error"`
		Description string `json:"error_description"`
	}
)

const (
	// ErrorInvalidRequest indicates that your API request was formatted incorrectly.
	//
	// Typically, this means that your request is missing some required value(s).
	ErrorInvalidRequest = "invalid_request"

	// ErrorUnauthorized indicates that you do not have access to the requested resource.
	//
	// There are many reasons you might see this error. It is possible that your API key does
	// not have the appropriate scopes, or if you are using an auth token, it may have expired.
	ErrorUnauthorized = "unauthorized"
)
