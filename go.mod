module vita.dev/clique

go 1.13

require (
	github.com/google/uuid v1.1.1
	gitlab.com/vita/go/clique v0.1.0
)
