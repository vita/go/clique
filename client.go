/*
Package clique helps you build Go applications that interact with the Clique API.

To get started, you will first need to get an API key. You can create a new API key
by visiting the [Security settings page](https://clique.app/settings/security) on the
Clique website. An API key is sensitive, like your account password, and should never
be checked into version control systems or shared online.

Once you have a key, you can create your first program using our Go SDK.

	package main

	import (
		"os"

		"vita.dev/clique"
	)

	func main() {
		client := clique.NewClient(os.Getenv("CLIQUE_API_KEY"))
		// do stuff
	}

Right now, the functionality of the SDK is rather limited. Future versions will enable you
to create posts, join groups, participate in conversations, and more.
*/
package clique // import "vita.dev/clique"

type (
	// Client represents a Clique API client.
	Client struct {
		APIKey string
	}
)

// NewClient creates a new Clique client.
func NewClient(apiKey string) *Client {
	return &Client{APIKey: apiKey}
}
