package clique

import "time"

type (
	// Post represents content shared on Clique.
	Post struct {
		ID        string                 `json:"string"`
		Type      string                 `json:"type"`
		Data      map[string]interface{} `json:"data"`
		Author    *User                  `json:"author,omitempty"`
		Audience  *Audience              `json:"audience"`
		Parent    *Post                  `json:"parent,omitempty"`
		Children  []Post                 `json:"children"`
		CreatedAt time.Time              `json:"created_at"`
		UpdatedAt time.Time              `json:"updated_at"`
	}

	// GetPostsInput is used to customize the query to the `/posts` API.
	GetPostsInput struct {
		AudienceID string `json:"audience_id"`
		AuthorID   string `json:"author_id"`
		OrderBy    string `json:"order_by"`
	}
)

const (
	// PostTypeImage represents a single image.
	PostTypeImage = "IMAGE"

	// PostTypeLink contains a URL shared by the post author.
	PostTypeLink = "LINK"

	// PostTypeThought represents a simple text post.
	PostTypeThought = "THOUGHT"
)
