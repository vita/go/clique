package clique

import "fmt"

// Endpoint returns a Clique API endpoint.
func Endpoint(path string) string {
	return fmt.Sprintf("https://api.clique.app%s", path)
}
