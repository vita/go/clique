package clique

import "time"

type (
	// Badge represents a user badge.
	Badge struct {
		ID          string    `json:"id"`
		Title       string    `json:"title"`
		Description string    `json:"description"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	}

	// EmailDetails contains information about an email address.
	EmailDetails struct {
		Email      string    `json:"email"`
		CreatedAt  time.Time `json:"created_at"`
		UpdatedAt  time.Time `json:"updated_at"`
		VerifiedAt time.Time `json:"verified_at"`
	}

	// PhoneDetails contains information about a phone number.
	PhoneDetails struct {
		Phone      string    `json:"phone"`
		CreatedAt  time.Time `json:"created_at"`
		UpdatedAt  time.Time `json:"updated_at"`
		VerifiedAt time.Time `json:"verified_at"`
	}

	// User represents a Clique member.
	User struct {
		ID          string         `json:"id"`
		Name        string         `json:"name"`
		NameGiven   string         `json:"given_name"`
		NameMiddle  string         `json:"middle_name"`
		NameFamily  string         `json:"family_name"`
		NameDisplay string         `json:"display_name"`
		Birthdate   string         `json:"birthdate"`
		Gender      string         `json:"gender"`
		Picture     string         `json:"picture"`
		Email       string         `json:"email"`
		Emails      []EmailDetails `json:"emails"`
		Phone       string         `json:"phone"`
		Phones      []PhoneDetails `json:"phones"`
		CreatedAt   time.Time      `json:"created_at"`
		UpdatedAt   time.Time      `json:"updated_at"`
	}
)

const (
	// GenderFemale indicates the user is female.
	GenderFemale = "female"

	// GenderMale indicates the user is male.
	GenderMale = "male"

	// GenderOther indicates the user is non-binary or gender-fluid.
	GenderOther = "other"
)
