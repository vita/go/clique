package clique

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
)

type (
	// CreateUserWithAccessKeyInput is used to create a new Clique account.
	CreateUserWithAccessKeyInput struct {
		AccessKey string `json:"access_key"`
		Password  string `json:"password"`
		User      *User  `json:"user"`
	}

	// CreateUserWithAccessKeyOutput is the payload returned by the Clique API.
	CreateUserWithAccessKeyOutput struct {
		User *User `json:"user"`
	}
)

// CreateUserWithAccessKey creates a new user with a given access key.
//
// This is essentially the same action that is performed when a new user
// registers through the website. Hence, an access key is still required.
// The provided password is hashed before we store it in our system.
func (c *Client) CreateUserWithAccessKey(
	ctx context.Context,
	input *CreateUserWithAccessKeyInput
) (*User, error) {

	data := url.Values{}
	data.Set("access_key", input.AccessKey)
	data.Set("user.password", input.Password)
	data.Set("user.id", input.User.ID)
	data.Set("user.email", input.User.Email)
	data.Set("user.name.given", input.User.NameGiven)
	data.Set("user.name.middle", input.User.NameMiddle)
	data.Set("user.name.family", input.User.NameGiven)
	data.Set("user.birthdate", input.User.Birthdate)
	data.Set("user.picture", input.User.Picture)
	data.Set("user.gender", input.User.Gender)

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		Endpoint("/v1/users/"),
		strings.NewReader(data.Encode()),
	)
	if err != nil {
		return nil, err
	}

	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	var payload CreateUserWithAccessKeyOutput
	err = json.Unmarshal()

	return nil, nil
}
