package clique_test

import (
	"testing"

	"gitlab.com/vita/go/clique"
)

func TestNewClient(t *testing.T) {
	client := clique.NewClient("hello-world")

	if client == nil {
		t.Error("expected *clique.Client, got nil")
	}
}
